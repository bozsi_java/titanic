import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def get_prefix(name):
    if 'Mr.' in name:
        return 'Mr.'
    elif 'Mrs.' in name:
        return 'Mrs.'
    elif 'Miss.' in name:
        return 'Miss.'
    else:
        return ''


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    df['prefix'] = df['Name'].apply(get_prefix)
    df['age_na'] = df.isna().Age
    missing = df.groupby('prefix')['age_na'].sum()
    medians = df.groupby('prefix')['Age'].median()
    my_sol = pd.DataFrame([missing, medians])

    return my_sol
